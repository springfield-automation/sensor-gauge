
import moment from 'moment';
import { Registry, Gauge } from 'prom-client';
import { Converter } from '@springfield/sensor';

export class SensorGauge {
  private gauge: Gauge;
  private converter: Converter<number>;

  constructor(name: string, help: string, converter: Converter<number>, registry: Registry) {
    this.converter = converter;
    this.gauge = new Gauge({name, help, registers: [registry]});
  }

  protected setGauge(value: number) {
    const sampleTime = moment().milliseconds(0);
    this.gauge.set(this.converter.convert(value), sampleTime.valueOf());
  }
}
