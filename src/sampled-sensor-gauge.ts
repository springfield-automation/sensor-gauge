import moment from 'moment';
import { Registry } from 'prom-client';
import { Sensor, Converter } from '@springfield/sensor';
import { SensorGauge } from './sensor-gauge';

export class SampledSensorGauge extends SensorGauge {
  private lastSampleTime: moment.Moment;
  private sensor: Sensor<number>;

  constructor(name: string, help: string, sensor: Sensor<number>, converter: Converter<number>, registry: Registry) {
    super(name, help, converter, registry);
    this.sensor = sensor;
    this.lastSampleTime = moment().seconds(0).milliseconds(0);
  }

  public sample() {
    const sampleTime = moment().seconds(0).milliseconds(0);

    if(!sampleTime.isSame(this.lastSampleTime)) {
      this.sensor.getCurrentValue().then((value) => {
        this.setGauge(value);
      });

      this.lastSampleTime = sampleTime;
    }
  }
}
