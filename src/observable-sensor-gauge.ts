import { Registry } from 'prom-client';
import { ObservableSensor, Converter } from '@springfield/sensor';
import { SensorGauge } from './sensor-gauge';

export class ObservableSensorGauge extends SensorGauge {
  constructor(name: string, help: string, sensor: ObservableSensor<number>, converter: Converter<number>, registry: Registry) {
    super(name, help, converter, registry);

    sensor.addObserver((error, value) => {
      if(!error) {
        this.setGauge(value);
      }
    });

    sensor.getCurrentValue().then(value => this.setGauge(value));
  }
}
