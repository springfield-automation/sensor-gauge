import { Converter } from '@springfield/sensor';

export class UnityConverter<T> implements Converter<T> {
  convert(value: T): T {
    return value;
  }
}
